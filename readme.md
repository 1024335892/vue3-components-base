# 文档规范使用

- 需要编译pnpm run build后的组件才能在example中进行使用验证
- 组件在文件夹vue3-components-base/src/widgets用大驼峰命名组件文件夹，组件同文件夹名，然后index.ts将组件给导出去

# example的举例使用注意事项

- package.json中依赖于@yll10243/vue3-components-base

# 编译后的组件直接可以发布了

- pnpm login
- pnpm publish

