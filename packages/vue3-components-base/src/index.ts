import type { App } from "vue"

import { AyiBoolean } from "./widgets/AyiBoolean"
export { AyiBoolean }

export default {
	install(app: App): void {
		app.component("AyiBoolean", AyiBoolean)
	}
}
