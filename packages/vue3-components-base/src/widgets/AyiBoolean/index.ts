import AyiBoolean from "./AyiBoolean.vue"
import type { App } from "vue"

export { AyiBoolean }

export default {
	install(app: App) {
		app.component("AyiBoolean", AyiBoolean)
	}
}
