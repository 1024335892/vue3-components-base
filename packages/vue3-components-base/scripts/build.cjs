const path = require("path")
const fs = require("fs-extra")
const dts = require("vite-plugin-dts")
const generateDTS = require("./generateDTS.cjs")
const rollupOptions = {
	external: ["vue", "element-plus"],
	output: {
		globals: {
			vue: "Vue",
			"element-plus": "elementPlus"
		}
	}
}
const config = {
	plugins: [
		dts({
			outputDir: "./dist/types",
			insertTypesEntry: false,
			copyDtsFiles: true
		})
	],
	build: {
		rollupOptions,
		minify: `terser`, // boolean | 'terser' | 'esbuild'
		sourcemap: false, // 输出单独 source文件
		lib: {
			entry: "./src/index.ts",
			name: "index",
			fileName: "index",
			formats: ["es", "cjs", "umd", "iife"]
		},
		outDir: "./dist"
	}
}
const widgets = path.resolve(__dirname, "../src/widgets")
const { build } = require("vite")

function componentsDirFun() {
	return fs.readdirSync(widgets).filter((name) => {
		const componentDir = path.resolve(widgets, name)
		const isDir = fs.lstatSync(componentDir).isDirectory()
		return isDir && fs.readdirSync(componentDir).includes("index.ts")
	})
}

function generateIndexTs() {
	let tempStr = `
import type { App } from "vue"
`
	let exportDefaultStart = `
export default {
	install(app: App): void {
`
	let exportDefaultEnd = `
 }
}
`
	let importStr = ""
	let exportArr = []
	let appComponentStr = ""
	const componentsDir = componentsDirFun()
	for (let item of componentsDir) {
		importStr += `import { ${item} } from "./widgets/${item}";`
		exportArr.push(item)
		appComponentStr += `app.component("${item}", ${item});`
	}
	const tempData = `
${tempStr}
${importStr}
export { ${exportArr.join()} };
${exportDefaultStart}
${appComponentStr}
${exportDefaultEnd}
`
	fs.outputFile(path.resolve(__dirname, "../src/index.ts"), tempData)
}
/**
 * 删除dist目录
 */
function rmDistDir() {
	const distDir = path.resolve(__dirname, "../dist")
	if (fs.existsSync(distDir)) {
		fs.rmdirSync(distDir, { recursive: true, force: true })
	}
}

const buildSingle = async () => {
	const baseOutDir = config.build.outDir
	generateIndexTs() //生成/src/index.ts文件
	rmDistDir() //删除dist之前编译的文件
	await build(config)
	// 复制 Package.json 文件
	const packageJson = require("../package.json")
	packageJson.main = "index.umd.cjs"
	packageJson.module = "index.js"
	packageJson.types = "index.d.ts"
	fs.outputFile(path.resolve(baseOutDir, `package.json`), JSON.stringify(packageJson, null, 2))

	// 拷贝 README.md文件
	// fs.copyFileSync(path.resolve("./README.md"), path.resolve(baseOutDir + "/README.md"))

	/**
	 * 或指定目录下的有index.ts文件的文件夹
	 */
	const componentsDir = componentsDirFun()
	for (let name of componentsDir) {
		const outDir = path.resolve(baseOutDir, name)
		const custom = {
			minify: `terser`,
			sourcemap: false,
			lib: {
				entry: path.resolve(widgets, name),
				name, // 导出模块名
				fileName: `index`,
				formats: ["es", "cjs", "umd", "iife"]
			},
			outDir
		}
		Object.assign(config.build, custom)
		await build(config)
		fs.outputFile(
			path.resolve(outDir, `package.json`),
			`{
        "name": "@yll10243/vue-form-render/${name}",
		"type": "module",
        "main": "index.umd.cjs",
        "module": "index.js"
        }`,
			`utf-8`
		)
	}
	//合并生成的d.ts文件
	generateDTS("../dist/index.js")
}
buildSingle()
