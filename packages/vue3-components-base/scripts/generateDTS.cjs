const path = require("path")
const fs = require("fs-extra")
function generateCode(meta, filePath) {
	const componentsArr = meta?.components ?? []
	if (componentsArr.length > 0) {
		let tempContent = `
            export * from './types/src/index'
            import ayiUI from './types/src/index'
            export default ayiUI
            declare module 'vue' {
                export interface GlobalComponents {
        `
		for (let item of componentsArr) {
			tempContent += `${item.component}:typeof import("./types/src/index").${item.component};`
		}
		tempContent += `
                }
            }
        `
		fs.writeFileSync(filePath, tempContent)
	}
}

async function getComponents(input) {
	const entry = await import(input)
	const res = Object.keys(entry)
		.filter((k) => k !== "default")
		.map((k) => ({
			component: k
		}))
	return res
}

async function generateDTS(entryPath) {
	const dts = path.resolve(__dirname, entryPath.replace(".js", ".d.ts"))
	// 组件库数据
	const components = await getComponents(entryPath)
	// 生成模版
	generateCode({ components }, dts)
}
module.exports = generateDTS
