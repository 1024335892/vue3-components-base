import { defineConfig, UserConfig } from "vite"
import vue from "@vitejs/plugin-vue"
import dts from "vite-plugin-dts"
// import vueJsx from "@vitejs/plugin-vue-jsx";
// https://vitejs.dev/config/
const rollupOptions = {
	external: ["vue", "vite-plugin-dts"],
	output: {
		globals: {
			vue: "Vue"
		}
	}
}

export const config = {
	plugins: [
		vue(),
		dts({
			outputDir: "./dist/types",
			insertTypesEntry: false,
			copyDtsFiles: true
		})
	],
	build: {
		rollupOptions,
		minify: `terser`, // boolean | 'terser' | 'esbuild'
		sourcemap: false, // 输出单独 source文件
		lib: {
			entry: "./src/index.ts",
			name: "index",
			fileName: "index",
			formats: ["es", "cjs", "umd", "iife"]
		},
		outDir: "./dist"
	}
}

export default defineConfig(config as UserConfig)
